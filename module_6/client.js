var socketio = io.connect();
var users = [];
var open_groups = [];
var pass_groups = [];
var private_messages = [];
var current_group = 'home';
var username;

/*

Server Listeners

 */

socketio.on("message_to_client",function(data) {
    //Append an HR thematic break and the escaped HTML of the new message
    document.getElementById("chatlog").appendChild(document.createElement("hr"));
    var text = data['user'] + ": " + data['message'];
    document.getElementById("chatlog").appendChild(document.createTextNode(text));
    $('.mid-panel').stop().animate({
        scrollTop: $("#chatlog")[0].scrollHeight
    }, 800);
});

socketio.on("username", function(data){
    username = data['username'];
    $('#username').html(data['username']);
});

socketio.on("users_refresh", function(data){
    updateUsers(data);

});

socketio.on("groups_refresh", function(data){
   console.log(data);
   updateGroups(data);
   reselected();
});

socketio.on("room_pass", function(data){
    var pass = data['password'];
    var room = data['room'];
    var attempt = prompt("This room is password protected");

    if(pass == attempt){
        joinRoom(room);
    }else{
        alert('Access Denied');
    }
});

socketio.on("private_message", function(data){
    alert("You Have Mail");
   var title = data['user'];
   var message = data['message'];
    $("#private-messages").append(
        '<span class="new_private_message">'+
        '<a>' + title + ' says: ' + message + '</a>'+
        '<hr>'+
        '</span>'
    );


    $('#private-messages').stop().animate({
        scrollTop: $("#private-messages")[0].scrollHeight
    }, 800);

    $('.new_private_message').on("swipe", function(event){
        debugger;
        $(event.target).fadeOut("slow","linear",function(){
            this.remove();
        });
    })
});

socketio.on('change_room', function(data){
   $('#chatlog').html('');
   current_group= data['room'];
   reselected();
});

socketio.on('update', function(data){
   socketio.emit('get_users', {room: current_group});
});

/*

Callback Functions

 */

// Rebuilds userlist based on information returned from the server
var updateUsers = function(data){
    $('#current_users').html('<p id="your_name" class="bg-info text-center"><span id="username"></span><a onClick="changeName()"> (Change Name?)</a></p>');
    $('#username').html(username);
    users = data['users'];
    for( var name in data['users']) {
        console.log(name);
        $("#current_users").append('<span class=\"glyphicon glyphicon-envelope pm_user\" data-value=\"'+ data['users'][name] +'\"></span><a data-value=\"'+ data['users'][name] +'\">' + data['users'][name] + '</a>');
        $("#current_users").append('<hr>');
    }

    //Double Click Listener for PMing a user
    $('.pm_user').on('click', function(event){
        pmUser($(event.target).data('value'));
    });
}

var kickUser = function(){
    var kickerName = username;
    var roomName = current_group;
    var kickeeName = $('#kick_input');
    socketio.emit('kick_user', {kicking:kickerName, kicked: kickeeName.val(), room: roomName});
    kickeeName.val('');
}

var banUser = function(){
    var kickerName = username;
    var roomName = current_group;
    var kickeeName = $('#kick_input');

    socketio.emit('ban_user', {kicking:kickerName, kicked: kickeeName.val(), room: roomName});
    kickeeName.val('');

}


//Rebiulds chatrooms based on information returned from the server
var updateGroups = function(data){
    $('#current_chatrooms').html('');
    open_groups = data['open_groups'];
    pass_groups = data['pass_groups'];
    for ( var name in open_groups){
        $("#current_chatrooms").append('<a class="join_room room_tag" data-value=\"'+ open_groups[name] +'\">' + open_groups[name] + '</a>');
        $("#current_chatrooms").append('<hr>');
    }

    for ( var name in pass_groups){
        $("#current_chatrooms").append('<a class="join_pass_room room_tag" style="color:red" data-value=\"'+ pass_groups[name] +'\">' + pass_groups[name] + '</a>');
        $("#current_chatrooms").append('<hr>');
    }

    //On Click Listeners for entering chatrooms
    $('.join_room').on('click', function(event){
        var target = $(event.target).data('value');
        if ( target != current_group)
            joinRoom($(event.target).data('value'));
    });

    $('.join_pass_room').on('click', function(event){
        var target =  $(event.target).data('value');
        if ( target != current_group)
            socketio.emit('get_room_password', {room: target});
    });
}

var reselected = function(){
    var rooms_array = $('.room_tag');
    for ( var index = 0; index < rooms_array.length; ++index){
        var tag = $(rooms_array[index]);
        if ( tag.data('value') == current_group){
            tag.addClass('selected_group');
        }else{
            tag.removeClass('selected_group');
        }
    }
}

var pmUser = function(name){
    var message = prompt("What would you like to send?");
    socketio.emit("private_message", {name: name, message: message});
}

var joinRoom = function(name){
    socketio.emit("join_room", {room: name, prev: current_group});
   // current_group = name;
   // $('#chatlog').html('');
   // $('#chatlog').append('<p style="color: blue">You have now entered ' + name + '</p>');

}

var createRoom = function(){
    var list = open_groups.concat(pass_groups);
    var name = $('#room_name').val();
    var pass = $('#room_pass').val();
    $('#room_name').val('');
    $('#room_pass').val('');
    if(list.indexOf(name) != -1){
        alert("Room Exists");
        return false;
    }

    if (!name && !pass){
        alert('Invalid');
    } else if( !pass) {
        socketio.emit('create_room', {prev: current_group, name: name, owner: username, password: '', blacklist: []});
    } else {
        socketio.emit('create_room', {prev: current_group, name: name, owner: username, password: pass, blacklist: []});
    }
}

var changeName = function(){
    var username = prompt("enter username");
    var valid = true;
    if(!username){
        valid = false
    }
    for( var name in users){
        if(users[name] == username){
            valid = false
        }
    }
    if (valid){
        socketio.emit("new_username", {username:username, room: current_group});
    }else{
        alert('Invalid Username');
    }
}

var sendMessage = function(){
    var message_el = document.getElementById("message_input");
    var msg = message_el.value;
    if(msg){
        socketio.emit("message_to_server", {room: current_group, message:msg});
        message_el.value = "";
    }
}

function submitOnEnter(event) {
    if (event.keyCode == 13) { // No need to do browser specific checks. It is always 13.
        sendMessage();
    }
}