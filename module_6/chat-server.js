// Require the packages we will use:
var http = require("http"),
    url = require('url'),
    path = require('path'),
    mime = require('mime'),
	socketio = require("socket.io"),
	fs = require("fs");

/*
 Server Magic Begins

  */
// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
	// This callback runs when a new connection is made to our HTTP server.
    var filename = path.join(__dirname, url.parse(req.url).pathname);
    if(filename == "/Users/DevScrum/CSE330group/calliet-423674-borstelmann-423159/module_6/" || filename == "/home/calliet.d/cse330group/calliet-423674-borstelmann-423159/module_6/"){
        fs.readFile("client.html", function(err, data){
            // This callback runs when the client.html file has been read from the filesystem.

            if(err) return resp.writeHead(500);
            resp.writeHead(200);
            resp.end(data);
            console.log("Serving: /home");
        });
    } else {
        fs.readFile(filename, function(err, data){
            // This callback runs when the client.html file has been read from the filesystem.

            if(err) return resp.writeHead(500);
            resp.writeHead(200);
            resp.end(data);
            console.log("Serving: " + filename);
        });
    }


console.log("Serving: " + filename);
});
app.listen(3456);
console.log("Serving on port: 3456");

/*

Server Magic ends

 */
//----------------------------------------------------------------------
/*

 users->user ID's
 users_socket->sockets of users indexed by their ids
 names = names of users indexed by their ids
 rooms = room object consisting of room name, owner id, password (optional) and the rooms blacklist (of user ids)

 */
var users = [];
var users_socket = {};
var names = {};
var rooms = {'home': {owner:'', password:'', blacklist:[]}, 'moderators': {owner:'', password:'enter', blacklist:[]}};

/*

 Helper and utility functions

 */

//Return a users ID by inputted name
var getIDbyName = function(name){
    for ( var key in names){
        if ( names[key] == name){
            return key;
        }
    }
    return null;
}

//Get List of Users
var getUsers = function(name, room){
    var user_list = [];
    var room_obj = room[name];
    for(var key in room_obj){

        if(names[key])
            user_list.push(names[key]);
    }
    return user_list;
}

//Get List of Groups
var getGroups = function(){
    var open = [];
    var pass = [];
    for ( var index in rooms){
        if(rooms[index].password){
            pass.push(index);
        }else{
            open.push(index);
        }
    }
    return [open, pass];
}

// Do the Socket.IO magic:
var io = socketio.listen(app);
io.sockets.on("connection", function(socket){
	//Logs into the home chatroom and builds the page

    //begin initial page setup
    var username = "GuestNo" + Math.floor(Math.random()*1000);
    console.log("New Guest Joined:" + username);
    socket.emit("username",{username:username});
    socket.join('home');
    users.push(socket.id);
    names[socket.id] = username;
    users_socket[socket.id] = socket;
    //Gets every user in the home room, and updates their active user list
    io.to('home').emit("users_refresh", {users:getUsers('home', io.sockets.adapter.rooms)});
    var grps = getGroups();
    io.sockets.emit("groups_refresh", {open_groups:grps[0], pass_groups: grps[1]});
    // end initial page setup

    /*

    Params-> name: 'room name' owner: 'id of room creater' pass: 'optional room pass' blacklist: empty array
    Builds room object
    Moves user to his/her new room
    Updates user
    Updates world on new event

     */

    socket.on('create_room', function(data){
        rooms[data['name']] = {owner: getIDbyName(data['owner']), password: data['password'], blacklist: data['blacklist']};
        socket.leave(data['prev']);
        socket.join(data['name']);
        socket.emit('change_room', {room: data['name']});
        var grps = getGroups();
        io.sockets.emit("groups_refresh", {open_groups:grps[0], pass_groups: grps[1]});
        io.sockets.emit("update");
    });


    /*

    Listening for join room event. params-> room: 'destination' prev: 'prev'
    Ensures a room change is occuring
    Ensure user can enter room ( not blacklisted )
    Preforms nessecary

     */
    socket.on('join_room', function(data){
        if(data['prev'] != data['room']){

            if(rooms[data['room']].blacklist.indexOf(socket.id) >= 0){
                socket.emit('change_room', {room: data['prev']});
                socket.emit("message_to_client", {user: 'system', message: 'you are banned from this room.'});

            }else{
                socket.leave(data['prev']);
                socket.join(data['room']);
                socket.emit('change_room', {room: data['room']});
                io.sockets.emit('update');
            }
        }
    });

    // Client looking to validate password
    // returns pass to server
    socket.on('get_room_password', function(data){
        var room = data['room'];
        socket.emit('room_pass',{password: rooms[room].password, room: room});
    });

    //User changes username
    socket.on('new_username', function(data){
        console.log(names[socket.id] + "is now known as " + data['username']);
        socket.emit("username", {username:data['username']});
        names[socket.id] = data['username'];
        var users = getUsers(data['room'], io.sockets.adapter.rooms);
        io.to(data['room']).emit('users_refresh', {users:users});
    });

    // Relaying a private message
    socket.on('private_message', function(data){
       var sender = names[socket.id];
       var message = data['message'];
        console.log("message sent from " + sender + " to " + data['name']);
        io.to(getIDbyName(data['name'])).emit("private_message", {user: sender, message: message});
    });

    //User sends message
    socket.on('message_to_server', function(data) {
        // This callback runs when the server receives a new message from the client.
        var room = data['room'];
        console.log(room + " -message: "+data["message"]); // log it to the Node.JS output
        io.to(room).emit("message_to_client",{user: names[socket.id], message:data["message"] }) // broadcast the message to other users
    });

    socket.on('kick_user', function(data){
        var kicking = data['kicking'];
        var kicked = data['kicked'];
        var room = data['room'];
        if(rooms[room].owner == getIDbyName(kicking)){
            var id = getIDbyName(kicked)
            if(id){
                users_socket[id].leave(room);
                users_socket[id].join('home');
                users_socket[id].emit('change_room', {room: 'home'});
                users_socket[id].emit('message_to_client',{user: 'system', message: 'You\'ve been kicked from '+room+'.'});
                io.sockets.emit("update");
            }else{
                socket.emit('message_to_client',{user: 'system', message: 'User no found'});
            }
        }else{
            socket.emit('message_to_client',{user: 'system', message: 'You don\'t have sufficient privileges.'});
        }
    });

    socket.on('ban_user', function(data){
        var kicking = data['kicking'];
        var kicked = data['kicked'];
        var room = data['room'];

        if(rooms[room].owner == getIDbyName(kicking)){
            var id = getIDbyName(kicked)
            if(id){
                users_socket[id].leave(room);
                users_socket[id].join('home');

                rooms[room].blacklist.push(id);
                console.log(room + "has added " + kicked + " to blacklist");
                users_socket[id].emit('change_room', {room: 'home'});
                users_socket[id].emit('message_to_client',{user: 'system', message: 'You\'ve been banned from '+room+'.'});

                io.sockets.emit("update");
            }else{
                socket.emit('message_to_client',{user: 'system', message: 'User no found'});
            }
        }else{
            socket.emit('message_to_client',{user: 'system', message: 'You don\'t have sufficient privileges.'});
        }
    });



    socket.on("disconnect", function(data){
        // deletes saved user from data
       users.splice(users.indexOf(socket.id), 1);
       console.log(names[socket.id] + " is disconnected");
       var name = names[socket.id];
       names[socket.id] = null;
       users_socket[socket.id] = null;
       for ( var index in rooms){
           console.log(rooms[index].owner);
           if(rooms[index].owner == getIDbyName(name)){
               console.log(index);
               io.to(index).leave(rooms[index]);
               io.to(index).join('home');
               io.to(index).emit("message_to_client", {user: 'system', message: 'creator has left the room.'});
               rooms[index] = 'null';
           }
       }
       io.sockets.emit("update");
    });


    socket.on('get_users', function(data){
        var users = getUsers(data['room'], io.sockets.adapter.rooms);
        io.to(data['room']).emit('users_refresh', {users:users});
    });

});