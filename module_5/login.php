<?php
header("Content-Type: application/json");
ini_set('display_errors', 'On');
session_start();
session_destroy();
if ($_POST['out'] == "false"){
    require 'sql_connect.php';

// Use a prepared statement
    $stmt = $mysqli->prepare("SELECT COUNT(*), username, password FROM users WHERE username=?");
    if(!$stmt){
        echo json_encode(array(
            "success" => false,
            "message" => "Can't connect to database"
        ));
        exit;
    }
    $user = htmlentities($_POST['user']);
    $pwd_guess = htmlentities($_POST['pass']);

// Bind the parameter
    $stmt->bind_param('s', $user);
    $stmt->execute();

// Bind the results
    $stmt->bind_result($cnt, $username, $pwd_hash);
    $stmt->fetch();
    $attempt = substr(crypt($pwd_guess, $pwd_hash), 0, strlen($pwd_hash));

// Compare the submitted password to the actual password hash
    if( $cnt == 1 && $attempt==$pwd_hash){
        // Login succeeded!
        session_start();
        $_SESSION['username']= $user;


        echo json_encode(array(
            "success" => true,
            "message" => "Logged In"
        ));
        exit;
    } else {

        echo json_encode(array(
            "success" => false,
            "message" => "Incorrect Username or Password"
        ));
        exit;
    }
}else {
    echo json_encode(array(
        "success" => true,
        "message" => "Logged Out"
    ));
    exit;
}


