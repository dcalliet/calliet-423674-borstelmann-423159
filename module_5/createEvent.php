<?php
header("Content-Type: application/json");
ini_set('display_errors', 'On');
session_start();
require 'sql_connect.php';

if (! $_SESSION['username']){
    echo json_encode(array(
        "success" => false,
        "message" => "Not Logged In"
    ));
    exit;
}

$owner = $_SESSION['username'];
$title = htmlentities($_POST['title']);
$date = htmlentities($_POST['date']);
$description = htmlentities($_POST['description']);

$stmt = $mysqli->prepare("INSERT INTO events (owner, title, date, description) VALUES (? , ?, ?, ?)");
 if(!$stmt){
 	echo json_encode(array(
 		"success" => false,
 		"message" => "Can't connect to database"
	));
 	exit;
 }
$stmt->bind_param('ssss', $owner, $title, $date, $description);
$stmt->execute();
$stmt->close();
echo json_encode(array(
	"success" => true,
	));
	exit;