<?php
header("Content-Type: application/json");
ini_set('display_errors', 'On');
session_start();
session_destroy();
require 'sql_connect.php';


$user = htmlentities($_POST['user']);
$pass = htmlentities($_POST['pass']);

$stmt = $mysqli-> prepare("SELECT username FROM users WHERE username=?");
if(!$stmt) {
    echo 'Query prep Failed:  ' . $mysqli->error;
    exit;
}

$stmt->bind_param('s', $user);
$stmt->execute();
$result = $stmt->get_result();
$row = $result->fetch_assoc();
if($row) {
    echo json_encode(array(
        "success" => false,
        "message" => "User already exists"
    ));
} else {
    $stmt = $mysqli->prepare("INSERT INTO users (username, password) VALUES (?, ?)");
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }

    session_start();
    $_SESSION['username'] = $user;

    $stmt->bind_param('ss', $user, crypt($pass));
    $stmt->execute();
    $stmt->close();
    echo json_encode(array(
        "success" => true,
        "message" => $user
    ));
    exit;
}
