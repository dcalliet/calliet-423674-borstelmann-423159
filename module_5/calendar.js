var event_data = [];
var current_time = new Date();
current_time.setDate(1);
var month = new Array();
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";
var day_of_wk = new Array();
day_of_wk[0] = "Sun";
day_of_wk[1] = "Mon";
day_of_wk[2] = "Tue";
day_of_wk[3] = "Wed";
day_of_wk[4] = "Thu";
day_of_wk[5] = "Fri";
day_of_wk[6] = "Sat";


// initial values for the current event
var selected_title = "Event Title", selected_des = "Event Description", selected_date = "Event Date";

/*
----------------------------------------------------------------------------------------

MAKE SURE USERS ARE NOT HARD CODED ANYWHERE BEFORE SUBMITTING DO NOT DELETE THIS MESSAGE

MAKE SURE TO GO BACK AND SALT THE PASSWORDS!!

 ----------------------------------------------------------------------------------------
*/

/*

    Adds Listeners when page loaded, if user logged in loads data

 */
var init = function(event){

    $('#login_form').on('submit', checkLogin);
    $('#register_form').on('submit', checkRegister);
    $('#event_form').on('submit', createEvent);
    $('.last_month').on('click', decreaseMonth);
    $('.next_month').on('click', increaseMonth);
    $('.title_month').html(month[current_time.getMonth()] + " " + current_time.getFullYear());
    buildMonth();
    loadData();
    currentEvent();

}

/*

    loads the users events from sql.

 */

var loadData = function(){
    var dataString = "loading=true";
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "getEvents.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load", loadDataCallback, false);
	xmlHttp.send(dataString);

}

function loadDataCallback(event){

        var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
        if (jsonData.success) {
            jsonData = JSON.parse(jsonData.result);
            cleanData(jsonData);
            $('#userInput').attr('disabled', 'disabled');
            $('#userInput').attr('placeholder', 'Signed In');
            $('#passInput').attr('disabled', 'disabled');
            $('#passInput').attr('placeholder', '');
            $('#signButton').addClass('signOut');
            $('#registerButton').attr('disabled', 'disabled');

        } else {
            $('#userInput').removeAttr('disabled');
            $('#userInput').attr('placeholder', 'Username');
            $('#passInput').removeAttr('disabled');
            $('#passInput').attr('placeholder', 'Password');
            $('#signButton').removeClass('signOut');
            $('#registerButton').removeAttr('disabled');

        }
}

/*

    formats data from the callback and saves it locally

 */

function cleanData(array){
    event_data = [];
    var index = 0;
    array.forEach(function(event){
        var var_date = new Date(event.date);
        var var_title = event.title;
        var var_desc = event.desc;
        var var_id = event.id;
        ++index;
        event_data.push({date: var_date, title: var_title, desc: var_desc, id: var_id});
    });

    buildEvents();
}


/*

    Creates event and posts it to sql, page refreshes after which loads data if logged in

 */

var createEvent = function(e){

	var title = $(e.target).find('#inputEventTitle').val();
	var date = $(e.target).find('#inputEventDate').val();
	var time = $(e.target).find('#inputEventTime').val();
	var datetime = new Date(date + " " + time).toISOString().slice(0, 19).replace('T', ' ');
	var description = $(e.target).find('#inputEventDescription').val();
	var dataString = "title=" + encodeURIComponent(title) + "&date=" + encodeURIComponent(datetime) + "&description=" + encodeURIComponent(description);
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "createEvent.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load", createEventCallback, false);
	xmlHttp.send(dataString);

}

function createEventCallback(event){
	var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
    console.log(jsonData);
}

/*

    Logs a user in

 */

var checkLogin = function(e){

	var username = $(e.target).find('#userInput').val();
	var password = $(e.target).find('#passInput').val();
    var out = $('#signButton').hasClass('signOut');
	var dataString = "user=" + encodeURIComponent(username) + "&pass=" + encodeURIComponent(password) + "&out=" + encodeURIComponent(out);
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "login.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load", loginCallback, false);
	xmlHttp.send(dataString);

}

function loginCallback(event){
	var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
	if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData;
		loadData(jsonData.user);
        alert( jsonData.message );
	}else{
        alert( jsonData.message);
    }

}

/*

    registers the user and logs the user in.

 */

var checkRegister = function(event) {
    var username = $('#userInput').val();
    var password = $('#passInput').val();
    var dataString = "user=" + encodeURIComponent(username) + "&pass=" + encodeURIComponent(password);
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", "register.php", true);
    xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xmlHttp.addEventListener("load", registerCallback, false);
    xmlHttp.send(dataString);
}

function registerCallback(event){
    var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
    if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData;
        var active_user = jsonData.message;
        $('#login_form').attr('disabled', 'disabled');
        $('#userInput').attr('placeholder', 'Logged In!');
        alert("Welcome! " + active_user + " thank you for joining, you have been logged in!");
    } else {
        alert( "You were not logged in.  " + jsonData.message );
    }
}

/*

    Allows movement along the calendar indefinitely

 */

var increaseMonth = function(){
    current_time.setMonth(current_time.getMonth() +1);
    $('.title_month').empty();
    $('.title_month').html(month[current_time.getMonth()] + " " + current_time.getFullYear());
    buildMonth();
}

var decreaseMonth = function(){
    current_time.setMonth(current_time.getMonth() -1);
    $('.title_month').empty();
    $('.title_month').html(month[current_time.getMonth()] + " " + current_time.getFullYear());
    buildMonth();
}

// finds the total number of days for a given month
function daysInMonth(month,year) {
    return new Date(year, month+1, 0).getDate();
}

/*

    Builds days for each month and year

 */

var buildMonth = function(){

    for ( var i = 1; i < 42; i ++) {
        $('.cal_' + i).empty();
    }
    var dayOfWeek = day_of_wk[current_time.getDay()];
    var monthLength = daysInMonth(current_time.getMonth(), current_time.getFullYear());
    var startBlock;
    switch(dayOfWeek) {
        case "Sun":
            startBlock = 1;
            break;
        case "Mon":
            startBlock = 2;
            break;
        case "Tue":
            startBlock = 3;
            break;
        case "Wed":
            startBlock = 4;
            break;
        case "Thu":
            startBlock = 5;
            break;
        case "Fri":
            startBlock = 6;
            break;
        case "Sat":
            startBlock = 7;
            break;
    }

    for ( var i = 1; i <= monthLength; i ++) {
        $('.cal_' + startBlock).html(i);
        startBlock++;
    }

    buildEvents();
}

/*

   Places any events that belong on the calendar

 */

var buildEvents = function(){
    event_data.forEach(function(event){
       var event_cell = getCellForEvent(event.date);
        if (event_cell) {
            var id_name =  event.title.replace(" ", "_") +'_'+ event_cell.replace(".","");
            $(event_cell).append('</br>'+
            '<span>'+
            '<span class="glyphicon glyphicon-remove" data-id="'+event.id+'" onClick="deleteEvent(this)" style="margin-right: 5px"></span>'+
            '<a class="event_href" id="'+id_name+'" data-title= "'+ event.title +'" data-content_time= "'+event.date +'" data-content_desc="'+ event.desc +'">'+event.title+'</a>'+

            '</span>'+
            '</br>');
            $('#' + id_name).off().on('click', function(){
                var $this = $(this)[0];
                selected_title = $('#' + $this.id).data('title');
                selected_date = $('#' + $this.id).data('content_time');
                selected_des = $('#' + $this.id).data('content_desc');
                currentEvent();
            });
        }
    });
}

function getCellForEvent(date){
    if(date.getMonth() != current_time.getMonth()){
        return false;
    } else {
        var startDate = new Date(date);
        startDate.setDate(0);
        var dayOfWeek = day_of_wk[startDate.getDay()];
        var startBlock;
        switch (dayOfWeek) {
            case "Sun":
                startBlock = 1;
                break;
            case "Mon":
                startBlock = 2;
                break;
            case "Tue":
                startBlock = 3;
                break;
            case "Wed":
                startBlock = 4;
                break;
            case "Thu":
                startBlock = 5;
                break;
            case "Fri":
                startBlock = 6;
                break;
            case "Sat":
                startBlock = 7;
                break;
        }
        var cell = startBlock + date.getDate();
        return ".cal_" + cell;
    }
}

/*

    Changes the HTML for the current event section in the main UI

 */

var currentEvent = function(){
    $('.describe_event_desc').html(selected_des);
    $('.describe_event_title').html(selected_title);
    $('.describe_event_time').html(selected_date);
}


/*

    Deletes an event that is selected

 */

var deleteEvent = function(target){
    if (confirm("Delete this Event? I cannot be recovered")){
        var id = $(target).data('id');
        var dataString = "id=" + id;
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("POST", "deleteEvent.php", true);
        xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlHttp.addEventListener("load", deleteCallback, false);
        xmlHttp.send(dataString);
    }
}

function deleteCallback(){
    location.reload();
}






