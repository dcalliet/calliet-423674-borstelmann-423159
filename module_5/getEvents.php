<?php
header("Content-Type: application/json");
session_start();
require 'sql_connect.php';

$stmt = $mysqli->prepare(" SELECT date, title, description, id FROM events WHERE owner=?");
 if(!$stmt){
 	echo json_encode(array(
 		"success" => false,
 		"message" => "Can't connect to database"
	));
 	exit;
 }
if(! $_SESSION['username']){
    echo json_encode(array(
        "success" => false,
        "message" => "Not Logged In"
    ));
    exit;
}
$user = htmlentities($_SESSION['username']);
$stmt->bind_param('s', $user);
$stmt->execute();
$rows = array();
// Bind the results
$stmt->bind_result($date, $title, $desc, $id);
while($stmt->fetch()){
    $arr = array ('date'=>$date, 'title'=>$title, 'desc'=>$desc, 'id'=>$id);
	$rows[] = $arr;
}
$jdata = json_encode($rows);

echo json_encode(array(
	"success" => true,
	"result" => $jdata
	));
exit;

