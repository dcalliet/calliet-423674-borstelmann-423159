<?php
header("Content-Type: application/json");
ini_set('display_errors', 'On');
session_start();
require 'sql_connect.php';

if (! $_SESSION['username']){
    echo json_encode(array(
        "success" => false,
        "message" => "Not Logged In"
    ));
    exit;
}

$owner = $_SESSION['username'];
$id = htmlentities($_POST['id']);

$stmt = $mysqli->prepare("DELETE FROM events WHERE owner=? AND id=?");
if(!$stmt){
    echo json_encode(array(
        "success" => false,
        "message" => "Can't connect to database"
    ));
    exit;
}
$stmt->bind_param('ss', $owner, $id);
$stmt->execute();
$stmt->close();
echo json_encode(array(
    "success" => true,
));
exit;