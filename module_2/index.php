<!DOCTYPE html>
    <head>
        <title>Calculator</title>
        <link rel="stylesheet" type="text/css" href="calculator.css"/>
    </head>
    <body>
        <form method="POST" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
            <div class="calc">
                <div class="inline position_ops">
                    <input type="radio" name="op" value="plus">Add <br>
                    <input type="radio" name="op" value="minus">Subtract <br>
                    <input type="radio" name="op" value="multiply"> Multiply <br>
                    <input type="radio" name="op" value="divide"> Divide <br>
                </div>
                <div class="inline position_body">
                    <div class="inline">
                        <input type="number" name="first" value="0">
                    </div>
                    <div class="inline">
                        <p>and</p>
                    </div>
                    <div class="inline">
                        <input type="number" name="second" value="0">
                        <input type="submit" value="Send">
                    </div>     
                </div>
            </div>
            
        </form>
        <div class="response">
            <?php
                if(isset($_POST['op'])){
                    $op = $_POST['op'];
                    $first = (float) $_POST['first'];
                    $second = (float) $_POST['second'];
                    $result = (float) 0;
                    if(! $first ){ $first = 0;}
                    if(! $second ) { $second = 0; }
                
                    switch($op) {
                        case "plus":
                            $result = $first + $second;
                        break;
                        case "minus":
                            $result = $first - $second;
                        break;
                        case "multiply":
                            $result = $first * $second;
                        break;
                        case "divide":
                            $result = $first / $second;
                        break; 
                    }
                    printf("<p><strong>%.2f is the result of %s</strong></p>\n",$result, $op);   
                    
                } else {
                    printf("Select an operation.");
                }
            ?>
        </div>
    </body>
</html>
    