<!DOCTYPE html>
<head>
	<title>Welcome to Fileshare</title>
	<link rel="stylesheet" type="text/css" href="login.css"/>
</head>


<body>
	<div class="header login">

		<div class="header title">
			<?php
				function validate() {
					/*
						$user & $pass retrieved from inputs in the form.
					*/
					$user = htmlentities($_POST['username']);
					$pass = htmlentities($_POST['password']);

					/*
						Reads user data array line by line from users.txt.
					*/
                    
					$txt = fopen("/home/dborstelmann/user_content/users.txt", "r");

					while ( ! feof($txt) ) {
						$info = fgets($txt);
						$data = json_decode($info, true);
						if($user == $data[0] && $pass == $data[1]){
							return true;
						}
					}
					return false;
				}


				/*
					If a logged in session exists, proceed.
				*/
				if(isset($_SESSION)){
					header("Location: fileshare.php");
					exit;
				}

				/*
					If data has been posted, run user validation.
				*/	
				if($_SERVER['REQUEST_METHOD'] == "POST"){
					if(validate()){
						session_start();
						$_SESSION['USER'] = htmlentities($_POST['username']);
						header("Location: fileshare.php");
						exit;
					}else {
						echo "<p> Welcome to Fileshare </p>
						<h6>Invalid Username or Password</h6>";						
					}
				}else

				echo "<p> Welcome to Fileshare </p>";
			?>
		</div>
		
	</div>

	<div class="body">
		<div class="body_left">
			<form name="logIn" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
				<label for="name">Username:</label>
				<input type="text" name="username"/><br>
				<label for="password">Password:</label>
				<input type="password" name="password"/><br>
				<input type="submit" name="Enter"/>
			</form>
		</div>
		<div class="body_right">
			<a href="forgotPassword.php">Forgot Password?</a><br>
			<a href="users.php"> Create New User</a>
		</div>
	</div>
</body>

</html>