<!DOCTYPE html>
<head>
	<title>Fileshare</title>
	<link rel="stylesheet" type="text/css" href="fileshare.css"/>
</head>
<body>
	<div class="header">
		<?php
			session_start();
			if (! isset($_SESSION['USER'])) {
				header('LOCATION: login.php');
				exit;
			}else 
				echo "<p>Welcome to Fileshare</p>";
		?>
	</div>

	<div class="body_left">
		<?php

			$dirname = "dir_" . $_SESSION['USER'];
			$filename = "/home/dborstelmann/user_content/" . $dirname . "/";


		function checkDir() {
			$dirname = "dir_" . $_SESSION['USER'];
			$filename = "/home/dborstelmann/user_content/" . $dirname . "/";

			if (!file_exists($filename)) {
				return mkdir($filename, 0777, true);
			}
			else return true;
		}

		function loadDir() {
			$dirname = "dir_" . $_SESSION['USER'];
			$filename = "/home/dborstelmann/user_content/" . $dirname  . "/";
            $download = "/home/dborstelmann/user_content/" . $dirname  . "/";
            
			$files = scandir($filename);
			$num_files = count($files);
            
            echo "<p> You have " . ($num_files - 2) . " files.</p>";
			echo "<ol>";
			for ( $cnt = 2; $cnt < $num_files; $cnt++){
				echo "<li><a href=readfile.php?path=" . $download . $files[$cnt] . ">" . $files[$cnt] ."</a>
                <a href=deletefile.php?path=" . $download . $files[$cnt] . "><u> delete</u></a></li>";
			}
			echo "</ol>";
		}

		if(checkDir()){
			loadDir();
		}else
			echo "<p>Error loading files</p>"
		?>
        

	</div>

	<div class="body_right">
        <div>
            <div style="display: inline-block">
            		<?php
                        $user = $_SESSION['USER'];
                        echo "<p>Logged in as " . $user . "</p>";
                    ?>
            </div>
            <div style="display: inline-block">
                    <form enctype="multipart/form-data" action="logout.php" method="POST">
                    <input type="submit" value="Logout" />
                    </form>
            </div>
        </div>
  
        <form enctype="multipart/form-data" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
	       <p>
		<input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
		<label for="uploadfile_input">Choose a file to upload:</label> <input name="uploadedfile" type="file" id="uploadfile_input" />
	       </p>
	       <p>
		<input type="submit" value="Upload File" />
	       </p>
        </form>
        
        <?php 
            if($_SERVER['REQUEST_METHOD'] == "POST"){
                uploadFile();
            }

            function uploadFile() {
                // Get the filename and make sure it is valid
                $filename = basename($_FILES['uploadedfile']['name']);

                // Get the username and make sure it is valid
                $username = "dir_" . $_SESSION['USER'];

                $full_path = "/home/dborstelmann/user_content/" . $username  . "/" . $filename;

                if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path) ){
                    chmod($fullpath, 0775);
                    header("Location: fileshare.php");
                    exit;
                }else{
                    header("Location: upload_failure.html");
                    exit;
                }
            }
        ?>
    </div>

</body>
</html>