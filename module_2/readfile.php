
<?php
        $file = $_GET['path'];

        if (file_exists($_GET['path'])) {
            header('Content-Description: File Transfer');
            
            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $mime = $finfo->file($file);
 
            header("Content-Type: ".$mime);
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    
?>
