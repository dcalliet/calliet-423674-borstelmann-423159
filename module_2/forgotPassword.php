<!DOCTYPE html>
<head>
	<title>Welcome to Fileshare</title>
	<link rel="stylesheet" type="text/css" href="forgotPassword.css"/>
</head>

<body>
	<div class="header login">
		<p>Welcome to Fileshare </p>
	<?php

		function validate() {
					/*
						$user & $pass retrieved from inputs in the form.
					*/
					$user = htmlentities($_POST['username']);
					$secret = htmlentities($_POST['secretQuestion']);

					/*
						Reads user data array line by line from users.txt.
					*/
					$txt = fopen("/home/dborstelmann/user_content/users.txt", "r");

					while ( ! feof($txt) ) {
						$info = fgets($txt);
						$data = json_decode($info, true);
						if($user == $data[0] && $secret == $data[2]){
							/*
								Using users inputted email address, send email if username and secret question are correct.
							*/
							//If running on Darius' EC2 instance
							//mail($data[3],"Fileshare password recovery","Your Password is \"" .$data[1]. "\" please keep this secure","From: fileshare@ec2-54-186-149-229.us-west-2.compute.amazonaws.com");
							//If running on Dan's EC2 instance
							mail($data[3],"Fileshare password recovery","Your Password is \"" .$data[1]. "\" please keep this secure","From: fileshare@ec2-54-201-210-176.us-west-2.compute.amazonaws.com");
							return true;
						}
					}
					return false;
		}

		if($_SERVER['REQUEST_METHOD'] == "POST"){
					if(validate()){
						echo "<h6> Password sent to email on file! </h6>";
					}else {
						echo "<h6> Invalid Username or Password </h6>";						
					}
		}else	echo "<h6> Lost your password? </h6>";	
	?>
	</div>
	<div class="body_left">
		<form name="password" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
				<label for="name">Username:</label>
				<input type="text" name="username"/><br>
				<label for="secretQuestion">Favorite Color?</label>
				<input type="text" name="secretQuestion"/><br>
				<input type="submit" name="Enter"/>	
		</form>
	</div>
	<div class="body_right">
		<a href="login.php">Login</a>
	</div>
</body>
</html>