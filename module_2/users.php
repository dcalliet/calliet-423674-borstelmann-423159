<!DOCTYPE html>
<head>
	<title> Welcome to FileShare </title>
	<link rel="stylesheet" type="text/css" href="users.css"/>
<head>

<body>
	<div class="header login">
		<p>Welcome to Fileshare </p>
		<?php 

			class User {
				public $username = "";
				public $password = "";
				public $secretQuestion = "";
				public $emailAddress = "";

			}
			function validate() {
				/*
					users retrieved from inputs in the form.
				*/
				$user = htmlentities($_POST['username']);

				/*
					Reads user data array line by line from users.txt.
				*/
				$txt = fopen("/home/dborstelmann/user_content/users.txt", "r");

				while ( ! feof($txt) ) {
					$info = fgets($txt);
					$data = json_decode($info, true);
					if($user == $data[0]){
						return true;
					}
				}
				return false;
			}

			function createNewUser() {
				$user = new User();
				$user->username = htmlentities($_POST['username']);
				$user->password = htmlentities($_POST['password']);
				$user->secretQuestion = htmlentities($_POST['secretQuestion']);
				$user->emailAddress = htmlentities($_POST['email']);

				//create a string to insert into the users text file.
				$USERINFO = "[ \"" . $user->username . "\", \"" . $user->password . "\", \"" . $user->secretQuestion . "\", \"" . $user->emailAddress . "\" ]\n";
				file_put_contents ("/home/dborstelmann/user_content/users.txt", $USERINFO, FILE_APPEND);

				loginUser();
			};

			function loginUser() {
                if (session_status() != PHP_SESSION_NONE) {
                    session_destroy();
                }
                session_start();
				header('Location: fileshare.php');
				exit();
			};

			if($_SERVER['REQUEST_METHOD'] == "POST"){
				if(validate()){
					echo "<h6>Sorry, that username is taken</h6>";
				} else

				createNewUser();
			}

		?>
	</div>
	<div class="body_left">
		<form name="logIn" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
					<label for="name">Username:</label>
					<input type="text" name="username"/><br>
					<label for="password">Password:</label>
					<input type="password" name="password"/><br>
					<label for="secretQuestion">Favorite Color?</label>
					<input type="text" name="secretQuestion"/><br>
					<label for="email">Email</label>
					<input type="email" name="email"/>

					<input type="submit" name="Enter"/>
		</form>
	</div>
	<div class="body_right">
		<a href="login.php">Login</a>
	</div>

<body>

</html>