<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="../include/css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="../css/register.css"/>
	<link rel="stylesheet" tpye="text/css" href="../css/all.css"/>
	<script type="text/javascript" href="../include/js/bootstrap.js"></script>
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script>
		var checkform = function() {
			var valid = 0,
			 	errorMessage = "";

			var user = $("#registerUser").val(),
				email = $("#registerEmail").val(),
				pass = $("#registerPass").val(),
				pass2 = $("#registerPass2").val();

			if ( user.length > 15 || user.length < 4) {
				++valid;
				errorMessage = "User must be between 4 and 15 characters";
			}

			if ( email.length > 25 || email.length < 4) {
				++valid;
				errorMessage = "Email must be between 4 and 15 characters";
			}

			if ( pass.length > 15 || pass.legth < 4 ){
				++valid;
				errorMessage = "Password must be between 4 and 15 characters";
			}

			if ( pass != pass2) {
				++valid;
				errorMessage = "Passwords do not match";
			}

			if(valid > 1){
				alert("You have an error in " + valid + "fields." );
				return false;
			} else if (valid > 0) {
				alert(errorMessage);
				return false;
			} else return true;

		}
	</script>
</head>
<body>

	<!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
               <a class="navbar-brand" href="main.php"> DDNews  - " Random News from Random People " </a>
            </div>
    </nav>

    <img src="../extras/newspapers.png" class="background" alt="background">

	<div class="jumbotron main">
		<h1>Welcome to DDNews</h1>
		<p>- help deliver more random news to even more random people</p>

		<form class="form-horizontal" role="form" action="register_check.php" method="POST" onSubmit="return checkform()">
		  <div class="form-group">
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="registerUser" placeholder="Username" style="margin-bottom: 4px" name="username">
		      <input type="email" class="form-control" id="registerEmail" placeholder="Email" name="email">
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-10">
				<input type="password" class="form-control" id="registerPass" placeholder="Password" style="margin-bottom: 4px" name="password">
		      	<input type="password" class="form-control" id="registerPass2" placeholder="Confirm Password">
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-10">
		      <button type="submit" class="btn btn-default">Register</button>
		    </div>
		  </div>
		</form>
	</div>

</body>

</html>