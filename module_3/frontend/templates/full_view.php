<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="../include/css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="../css/full_view.css"/>
	<link rel="stylesheet" tpye="text/css" href="../css/all.css"/>
	<script type="text/javascript" href="../include/js/bootstrap.js"></script>
    <title>DDNews</title>
</head>
<body>
	<!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="main.php">DDNews</a>
            </div>
            <div class="pull-right">
                
                <?php
                session_start();
                require "sql_connect.php";

                $guest = '
                <form class="navbar-form navbar-left" role="form" action="login.php" method="post">
                <div class="form-group">
                  <input type="text" placeholder="Username" class="form-control" name="user" id="emailInput">
                </div>
                <div class="form-group">
                  <input type="password" placeholder="Password" class="form-control" name="pass" id="passInput">
                </div>
                <button type="submit" class="btn btn-warning" name="action" value="Sign">Sign in</button>
                
                <button type="submit" class="btn btn-primary" name="action" value="Register">Register</button>
                </form>';

                $user = '
                <form id="log" class="navbar-form navbar-left" role="form" action="logout.php" method="post" style="display:inline-block">
                <div class="form-group">
                  <input type="text" placeholder="Have" class="form-control" name="user" id="emailInput" disabled="disabled">
                </div>
                <div class="form-group">
                  <input type="password" placeholder="Fun!" class="form-control" name="pass" id="passInput" disabled="disabled">
                </div>
                <button type="submit" class="btn btn-warning" name="action" value="out">Sign Out</button>
                </form>';
                

                    if (isset($_SESSION['user_id'])) {
                       echo $user;
                    } else {
                        echo $guest;
                    }
                ?>
            </div>
            <!--/.navbar-collapse -->
            <!-- Collect the nav links, forms, and other content for toggling -->
        </div>
        <!-- /.container -->
    </nav>

     <img src="../extras/newspapers.png" class="background" alt="background">

     <div class="jumbotron main less-top-padding">

        <?php
            //Check if recent navigation to this page
            if (isset($_POST['story_id'])){
                $_SESSION['story_id'] = htmlentities($_POST['story_id']);
            }
            //Loads latest instance Else there is an error
            if(isset($_SESSION['story_id'])){
                $story_id = htmlentities($_SESSION['story_id']);
            } else {
                echo "Error returning to page, please navigate to the homepage. " . $_SESSION['story_id'];
            }
            

            $stmt = $mysqli->prepare("SELECT u.username, s.subject, s.text, s.created, s.user_id FROM stories s JOIN users u ON (s.user_id = u.user_id) WHERE story_id=?");
            if(!$stmt) {
                echo 'Query prep Failed on SELECT:  %s\n' . $mysqli->error;
                exit;
            }
            $stmt->bind_param('s', $story_id);
            $stmt->execute();
            $stmt->bind_result($username, $sub, $text, $created, $userid);
            $stmt->fetch();

            $my_story = '<blockquote>
                <h2>' . $sub . '</h2>
                <footer> created by '. $username .' on '. $created .'</footer>
                </blockquote>

                <small>' . $text. '</small>
                <form class="navbar-form navbar-left" role="form" action="mod_post.php" method="post">
                <input type="hidden" name="subject" value="'.$sub.'">
                <input type="hidden" name="body" value="'.$text.'">
                <button type="submit" class="btn btn-warning" name="action" value="delete">Delete Post</button>
                <button type="submit" class="btn btn-warning" name="action" value="edit">Edit</button>
                </form>';

            $random_story = '<blockquote>
                <h2>' . $sub . '</h2>
                <footer> created by '. $username .' on '. $created .'</footer>
                </blockquote>

                <small>' . $text. '</small>';


            if ($userid == $_SESSION['user_id']) {
                echo $my_story;
            } else {
                echo $random_story;
            }
            
            $stmt->close();
            ?>


            <div class="jumbotron secondary min-top-padding ">
                <h4 style="text-align: center">Comments</h4>

                <div class="form-group" style="padding-bottom: 70px">
                            <div class="col-offset-2 col-sm-12">
                            <textarea form="post_comment" class="form-control" rows="3" name="body"></textarea> 

                <form id="post_comment" class="navbar-form navbar-left" role="form" action="post_comment.php" method="post">

            <?php

			$user = '
	     		<button type="submit" class="btn btn-large btn-block btn-primary" name="action" value="post_comment">Submit</button>';
            $guest = '
                <button type="submit" class="btn btn-large btn-block btn-primary" disabled="disabled">Sign in to Post Comments</button>';
            
            if (isset($_SESSION['user_id'])) {
                       echo $user;
                    } else {
                        echo $guest;
            }

            echo '
                        </form>
                    </div>
                </div>
                </div>
                        
        </div> 
            ';

            $stmt = $mysqli->prepare("SELECT text, created_by, created, user_id, comment_id FROM comments WHERE story_id = ? ORDER BY created desc ");
                    if ( !$stmt) {
                        echo "Query Prep Failed:" . $mysqli->error;
                        error;
                    }
                    $stmt->bind_param('s', $_SESSION['story_id']);
                    $stmt->execute();
                    $stmt->bind_result($body, $user, $created, $user_id, $comment_id);
                    while($stmt->fetch()) {
                    $random_comment = '
                        <li>
                            <div class="jumbotron secondary min-top-padding min-bottom-padding">

                            <h5>' .$body. '</h5>
                            <blockquote class="min-top-padding min-bottom-padding" style="margin-bottom: 5px">
                                <footer> comment by '.$user.' on ' . $created. '</footer>
                            </blockquote>
                            </div>
                        </li>
                    ';
                    $my_comment = '
                        <li>
                            <div class="jumbotron secondary min-top-padding min-bottom-padding">
                            <form class="navbar-form navbar-left" role="form" action="delete_comment.php" method="post">
                                <input type="hidden" name="comment_id" value="'.$comment_id.'">
                                <button type="submit" class="button btn btn-link">Delete</button>
                            </form>
                            <h5>' .$body. '</h5>
                            <blockquote class="min-top-padding min-bottom-padding" style="margin-bottom: 5px">
                                <footer> I wrote this on ' . $created. '</footer>
                            </blockquote>
                            
                            </div>
                        </li>
                    ';
                    echo '<ul style="list-style:none">';
                        if($_SESSION['user_id'] == $user_id){
                            echo $my_comment;
                        }else echo $random_comment;
                    }
                    echo '</ul>';

        ?>

</body>



</html>