<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="../include/css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="../css/register_check.css"/>
	<link rel="stylesheet" tpye="text/css" href="../css/all.css"/>
	<script type="text/javascript" href="../include/js/bootstrap.js"></script>
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
</head>

<body>


	<!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            Brand and toggle get grouped for better mobile display
            <div class="navbar-header">
               <a class="navbar-brand"> DDNews  - " Random News from Random People " </a>
            </div>
    </nav>
    <img src="../extras/newspapers.png" class="background" alt="background">
    <div class="jumbotron main">

    	<?php

            ini_set('display_errors', 'On');
    		require "sql_connect.php";
    		$user = htmlentities($_POST['username']);
    		$pass = htmlentities($_POST['password']);
            $stories = 0;


    		//check if this username is already contained 
    		$stmt = $mysqli-> prepare("SELECT username FROM users WHERE username=?");
            if(!$stmt) {
                echo 'Query prep Failed:  ' . $mysqli->error;
                exit;
            }
            
            $stmt->bind_param('s', $user);
            $stmt->execute();
            $result = $stmt->get_result();
            $row = $result->fetch_assoc();
            if($row) {
                echo '<h1>';
                echo $row['username'] . ' already exists';
                echo '</h1>';
                echo "<form action='register.php'>";
                echo '<button type="submit" class="btn btn-warning" onClick=register.php>Back To Registration</button>';
                echo '</form>';
            } else {
                $stmt = $mysqli->prepare("INSERT INTO users (username, password, num_stories) VALUES (?, ?, ?)");
                if(!$stmt){
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
                $stmt->bind_param('sss',$user,crypt($pass), $stories);
                $stmt->execute();
                $stmt->close();
                echo '<h1>';
                echo "Created User! You Can Return Home";
                echo '</h1>';
                echo "<form action='main.php'>";
                echo '<button type="submit" class="btn btn-success">Back To Home</button>';
                echo '</form>';
            }
    	?>

    </div>

</body>

</html>