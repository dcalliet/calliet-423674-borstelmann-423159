<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DDNews</title>

    <!-- Bootstrap Core CSS -->
    <link href="../include/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../include/css/3-col-portfolio.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/all.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php
    session_start();
    require "sql_connect.php";
?>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="main.php">DDNews</a>
            </div>
            <div class="pull-right">
                
                <?php
                $guest = '
                <form class="navbar-form navbar-left" role="form" action="login.php" method="post">
                <div class="form-group">
                  <input type="text" placeholder="Username" class="form-control" name="user" id="emailInput">
                </div>
                <div class="form-group">
                  <input type="password" placeholder="Password" class="form-control" name="pass" id="passInput">
                </div>
                <button type="submit" class="btn btn-warning" name="action" value="Sign">Sign in</button>
                
                <button type="submit" class="btn btn-primary" name="action" value="Register">Register</button>
                </form>';

                $user = '
                <form class="navbar-form navbar-left" role="form" action="logout.php" method="post" style="display:inline-block">
                <div class="form-group">
                  <input type="text" placeholder="Have" class="form-control" name="user" id="emailInput" disabled="disabled">
                </div>
                <div class="form-group">
                  <input type="password" placeholder="Fun!" class="form-control" name="pass" id="passInput" disabled="disabled">
                </div>
                <button type="submit" class="btn btn-warning" name="action" value="logout">Sign Out</button>';
                

                    if (isset($_SESSION['user_id'])) {
                       echo $user;
                    } else {
                        echo $guest;
                    }
                ?>
            </div>
            <!--/.navbar-collapse -->
            <!-- Collect the nav links, forms, and other content for toggling -->
        </div>
        <!-- /.container -->
    </nav>
    
    <img src="../extras/newspapers.png" class="background" alt="background">

    <!-- Page Content -->
    <div class="container">

        <!-- Page Header -->
        <div class="row">
            <div class="col-lg-8">
                <h1 class="page-header">DDNews
                    <small>Random news from random people.</small>
                </h1>
            </div>
            <div class="col-lg-4 post">
            <?php
                $user='
                <form role="form" action="new_post.php" method="post">
                    <button type="submit" class="btn btn-primary" name="action" value="new_post">New Post</button>
                </form>';
                if (isset($_SESSION['user_id'])){
                    echo $user;
                }
            ?>
            </div>
        </div>
        <!-- /.row -->

        <!-- Projects Row -->
       
            
                <?php

                    $stmt = $mysqli->prepare("select subject, text, created, story_id from stories order by created desc limit 10");
                    if ( !$stmt) {
                        printf("Query Prep Failed: %s\n", $mysqli->error);
                        error;
                    }

                    $stmt->execute();
                    $stmt->bind_result($subject, $body, $created, $id);
                    echo '<ul style="list-style:none">';
                    while($stmt->fetch()) {
                        echo '
                        <li>
                            <div class="row">
                                <div class="col-md-8 portfolio-item main-news">
                                    <h3>
                                        <a>'. $subject .'</a>
                                    </h3>
                                    <p>'. substr($body, 0, 400) .'...</p>
                                    <h6>Randomly posted on:'. $created .'</h6>
                                    <form action="full_view.php" method="POST">
                                        <input type="hidden" name="story_id" value=' . $id .'>
                                        <button type="submit" name="action" value="full_view">Read More</button>
                                    </form>
                                </div>
                            </div>
                        </li>
                        ';
                    }
                    echo '</ul>';
                ?>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; DDNews</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>
