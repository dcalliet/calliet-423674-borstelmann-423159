<?php
	session_start();
	require "sql_connect.php";

	if ($_POST['action'] == 'delete') {

		$stmt = $mysqli->prepare("DELETE FROM comments WHERE story_id=?");
            if(!$stmt) {
                echo 'Query prep Failed:  %s\n' . $mysqli->error;
                exit;
            }
            $stmt->bind_param('s', $_SESSION['story_id']);
            $stmt->execute();
            


		$stmt = $mysqli->prepare("DELETE FROM stories WHERE story_id=? AND user_id=?");
            if(!$stmt) {
                echo 'Query prep Failed:  %s\n' . $mysqli->error;
                exit;
            }
            $stmt->bind_param('ss', $_SESSION['story_id'], $_SESSION['user_id']);
            $stmt->execute();

        

        $stmt = $mysqli-> prepare("UPDATE users SET num_stories = num_stories - 1 WHERE user_id=?");
            if(!$stmt) {
                echo 'Query prep Failed:  %s\n' . $mysqli->error;
                exit;
            }
            
            $stmt->bind_param('s', $_SESSION['user_id']);
            $stmt->execute();
            $stmt->close();

            unset($_SESSION['story_id']);
            header("Location: main.php");
            exit;
            
	}

	if( $_POST['action'] == 'edit') {
		$_SESSION['subject'] = $_POST['subject'];
		$_SESSION['body'] = $_POST['body'];
		header("Location: new_post.php");
		exit;
	}
?>