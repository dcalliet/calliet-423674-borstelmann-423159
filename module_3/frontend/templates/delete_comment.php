<?php
	session_start();
	require "sql_connect.php";

	$comment_id = $_POST['comment_id'];

	$stmt = $mysqli->prepare("DELETE FROM comments WHERE comment_id=?");
            if(!$stmt) {
                echo 'Query prep Failed:  %s\n' . $mysqli->error;
                exit;
            }
            $stmt->bind_param('s', $comment_id);
            $stmt->execute();

            header('Location: full_view.php');
?>