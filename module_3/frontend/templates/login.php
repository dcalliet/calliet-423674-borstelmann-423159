<?php
// This is a *good* example of how you can implement password-based user authentication in your web application.
ini_set('display_errors', 'On');
session_start();
$story_id = htmlentities($_SESSION['story_id']);
session_destroy();
require 'sql_connect.php';

if( $_POST['action'] == "Register") {
	header("Location: register.php");
	exit;
} else
	{
		$previous = htmlentities($_SERVER['HTTP_REFERER']);
	}
 
// Use a prepared statement
$stmt = $mysqli->prepare("SELECT COUNT(*), user_id, password FROM users WHERE username=?");
 $user = htmlentities($_POST['user']);
 $pwd_guess = htmlentities($_POST['pass']);

// Bind the parameter
$stmt->bind_param('s', $user);
$stmt->execute();
 
// Bind the results
$stmt->bind_result($cnt, $user_id, $pwd_hash);
$stmt->fetch();
$attempt = substr(crypt($pwd_guess, $pwd_hash), 0, strlen($pwd_hash));

// Compare the submitted password to the actual password hash
if( $cnt == 1 && $attempt==$pwd_hash){
	// Login succeeded!
	session_start();
	$_SESSION['user_id'] = $user_id;
	$_SESSION['story_id']= $story_id;
	$_SESSION['username']= $user;

	// Redirect to your target page
	if(isset($_SESSION['user_id'])){
	    header("Location:" . $previous);
	} else echo "Weird error";
}else{
	// Login failed; redirect back to the login screen
	header("Location: main.php");
}
?>