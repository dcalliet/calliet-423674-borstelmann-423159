<?php
//This is a *good* example of how you can implement password-based user authentication in your web application.
ini_set('display_errors', 'On');
session_start();
if ( $_POST['action'] != 'logout' && $_POST['action'] != 'out'){
	header("location:" . htmlentities($_POST['action']). ".php");
	break;
}
require 'sql_connect.php';
$previous = htmlentities($_SERVER['HTTP_REFERER']);
 
session_destroy();
header("Location:" . $previous);
exit;
?>