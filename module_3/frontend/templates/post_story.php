    	<?php

            ini_set('display_errors', 'On');
    		require "sql_connect.php";
            session_start();
            if(!isset($_SESSION['user_id'])){
                //if session doesnt seem to be valid redirect
                header("Location: main.php");
            }

    		$subject = htmlentities($_POST['subject']);
    		$body = htmlentities($_POST['body']);
            $id = htmlentities($_SESSION['user_id']);

    		//check if this username is already contained 
    		$stmt = $mysqli-> prepare("UPDATE users SET num_stories = num_stories + 1 WHERE user_id=?");
            if(!$stmt) {
                echo 'Query prep Failed:  %s\n' . $mysqli->error;
                exit;
            }
            
            $stmt->bind_param('s', $id);
            $stmt->execute();
            $stmt->close();

            
            $stmt = $mysqli->prepare("INSERT INTO stories (user_id, subject, text) VALUES (?, ?, ?)");
                if(!$stmt){
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
            $stmt->bind_param('sss', $id, $subject, $body);
            $stmt->execute();
            $stmt->close();


            $stmt = $mysqli->prepare("SELECT story_id FROM stories WHERE user_id = ? AND subject = ?" );
                if(!$stmt){
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
            $stmt->bind_param('ss', $id, $subject);
            $stmt->execute();
            $stmt->bind_result($story_id);
            $stmt->fetch();
            $_SESSION['story_id'] = $story_id;
            $stmt->close();
            header("Location: full_view.php");
    	?>

    </div>

</body>

</html>