<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DDNews</title>

    <!-- Bootstrap Core CSS -->
    <link href="../include/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../include/css/3-col-portfolio.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/all.css" rel="stylesheet">
</head>

<body>
    <?php
        session_start();
        require "sql_connect.php";
    ?>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
                <a class="navbar-brand" href="main.php">DDNews</a>
        </div>
        <!-- /.container -->
    </nav>
    
    <img src="../extras/newspapers.png" class="background" alt="background">


    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-lg-8">
                <h1 class="page-header">Post
                    <small> your random news for these random people.</small>
                </h1>
            </div>

        </div>
        
        <div class="row">
            <div>

            <?php
            $newPost = '
                <form class="col-lg-8 new-post" role="form" action="post_story.php" method="post">
                <div class="form-group">
                  <input type="text" placeholder="Subject" class="form-control" name="subject">
                </div>
                <div class="form-group">
                    <textarea class="col-lg-8 form-control text-box" rows="10" name="body"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success post-button">Post</button>
                </div>
                </form>';
            $editPost = '<form class="col-lg-8 new-post" role="form" action="update_story.php" method="post">
                <div class="form-group">
                <a>'.$_SESSION["subject"].'
                  <input type="hidden" name="subject" value="'.$_SESSION['subject'].'">
                </div>
                <div class="form-group">
                    <textarea class="col-lg-8 form-control text-box" rows="10" name="body">'.$_SESSION['body'].'</textarea>
                </div>
                <div class="form-group">
                    <input type="hidden" name="story_id" value="'.$_SESSION['story_id'].'">
                    <button type="submit" class="btn btn-success post-button">Post</button>
                </div>
                </form>';

            if($_SERVER['REQUEST_METHOD'] == "POST"){
                echo $newPost;
            }elseif (isset($_SESSION['subject']) && isset($_SESSION['body'])) {
                echo $editPost;
            } else echo $newPost;
            ?>
            </div>
        </div>
        <!-- /.row -->
        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; DDNews</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
