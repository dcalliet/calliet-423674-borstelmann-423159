    	<?php

            ini_set('display_errors', 'On');
    		require "sql_connect.php";
            session_start();
            if(!isset($_SESSION['user_id'])){
                //if session doesnt seem to be valid redirect
                header("Location: main.php");
            }

    		$subject = htmlentities($_POST['subject']);
    		$body = htmlentities($_POST['body']);
            $id = htmlentities($_SESSION['user_id']);
            $story_id = htmlentities($_POST['story_id']);

    		//check if this username is already contained 
    		$stmt = $mysqli-> prepare("UPDATE stories SET subject = ?, text = ? WHERE story_id=?");
            if(!$stmt) {
                echo 'Query prep Failed:  %s\n' . $mysqli->error;
                exit;
            }
            
            $stmt->bind_param('sss', $subject, $body, $story_id);  
            $stmt->execute();
            $stmt->close();
            
            $_SESSION['story_id'] = $story_id;
            unset($_SESSION['body']);
            unset($_SESSION['subject']);

            header("Location: full_view.php");
    	?>

    </div>

</body>

</html>