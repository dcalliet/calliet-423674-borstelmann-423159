/**
 * Created by DevScrum on 10/31/14.
 */
module.exports = function(grunt) {
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    handlebars: {
          compile: {
              options: {
                  namespace: "TEMPLATES"
              },
              files: {
                  'app/static/include/compiledTemplates.js': ['app/templates/*.hbs']
              }
          }
    },
    watch: {
      handlebars: {
          files: 'app/templates/*.hbs',
          tasks: ['handlebars:compile', 'concat:first']
      },
      scripts: {
          files: ['app/static/js/*/*.js', 'Gruntfile.js'],
          tasks: ['concat:first', 'concat:js'],
          options: {
              spawn: false
          }
      },
      css: {
            files: 'app/static/css/*.css',
            tasks: ['concat:css']
      }
    },
    concat: {
      first: {
            src: [
                'app/static/js/require/jquery-1.11.1.min.js',
                'app/static/js/require/underscore-min.js',
                'app/static/js/require/backbone-min.js',
                'app/static/js/require/bootstrap.min.js',
                'app/static/js/require/handlebars-v2.0.0.js'
                ],
            dest: 'app/static/include/source.js'
            },
      js:   {
            src: ['app/static/js/custom/*.js'],
            dest: 'app/static/include/scripts.js'
            },
      css:  {
            src: ['app/static/css/*.css'],
            dest: 'app/static/include/build.css'
            }
    }
});

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-handlebars');
    grunt.registerTask('compile', ['handlebars', 'concat:first', 'concat:js','concat:css']);
    grunt.registerTask('push', ['handlebars', 'concat']);
    grunt.registerTask('default', ['compile', 'watch']);

};
