from app import db
from hashlib import md5
from flask import jsonify

class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(64), index=True, unique=True)
	email = db.Column(db.String(64), unique=True)

	def is_authenticated(self):
		return True

	def is_active(self):
		return True

	def is_anonymous(self):
		return False

	def get_id(self):
		try:
			return unicode(self.id)
		except NameError:
			return str(self.id)

	def json_view(self):
		return {"id": self.id, "name": self.name}

	def __repr__(self):
		return '<User ' + str(self.name) + ">"

class Resturant(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(64))
	price = db.Column(db.Integer)
	quality = db.Column(db.Integer)
	type = db.Column(db.String(64))
	community_score = db.Column(db.Integer)
	expert_score = db.Column(db.Integer)
	of_the_day = db.Column(db.Boolean, default=False)
	description = db.Column(db.String(64))
	review = db.relationship('Review', backref='ReviewTarget', lazy='dynamic')

	def json_view(self):
		return {'id': self.id, 
				"name": self.name, 
				"price": self.price, 
				"quality": self.quality, 
				"type": self.type,
				"community_score": self.community_score,
				"expert_score": self.expert_score,
				"description": self.description,
				"of_the_day": self.of_the_day}

	def __repr__(self):
		return '<Resturant %r>' % (self.name)

class Review(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	author = db.Column(db.String(64), db.ForeignKey('user.name'))
	resturant = db.Column(db.Integer, db.ForeignKey('resturant.id'))
	text = db.Column(db.String(500))

	def json_view(self):
		return {"id": self.id,
				"author": self.author,
				"resturant": self.resturant,
				"text": self.text}

	def __repr__(self):
		return 'Review stated ' + self.text + ', for resturant number ' + str(self.resturant) + ' by ' + self.author

class Staff(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(64))
	position = db.Column(db.String(64))

