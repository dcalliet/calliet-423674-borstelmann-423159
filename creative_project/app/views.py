from flask import render_template, flash, redirect, session, url_for, request, g, jsonify
from flask.ext.login import login_user, logout_user, current_user, login_required
from app import app, db, lm, oid
from forms import LoginForm, ResturantForm, OTDForm
from models import User, Resturant, Review
import json

tempStorage = {}

# Static Page Server

@app.route('/')
def index():
	return render_template('base.html')

# Login Page Flows

@app.before_request
def before_request():
	g.user = current_user
	if g.user.is_authenticated():
		db.session.add(g.user)
		db.session.commit()

@app.route('/login', methods=['GET','POST'])
@oid.loginhandler
def login():
	if g.user is not None and g.user.is_authenticated():
		return redirect(url_for('index'))
	form = LoginForm()
	# can only login through google
	oid_google = "https://www.google.com/accounts/o8/id"
	return oid.try_login(oid_google, ask_for=['nickname', 'email'])

@oid.after_login
def after_login(resp):

	if resp.email is None or resp.email == "":
		flash('Invalid login. Please try again.')
		return redirect(url_for('index'))
	new_user = User.query.filter_by(email=resp.email).first()
	if new_user is None:
		name = resp.nickname
		if name is None or name == "":
			name = resp.email.split('@')[0]

		new_user = User(name=name, email=resp.email)

		print new_user
		db.session.add(new_user)
		db.session.commit()
	login_user(new_user)
	return redirect(url_for("index"))

@lm.user_loader
def load_user(id):
	user = User.query.get(int(id))
	return user

@app.route('/check_login', methods=['GET', 'POST'])
def is_logged():
	return jsonify(logged_in=g.user.is_authenticated())	

@app.route('/logout', methods=['GET', 'POST'])
def logout():
	logout_user()
	return redirect(url_for("index"))


# handling of required data

@app.route('/resturants')
def resturant_list():
	resturants = Resturant.query.all()
	rest_array = []
	for r in resturants:
		rest_array.append(r.json_view())
	return jsonify(resturants=rest_array)

@app.route('/front_page', methods=["GET","POST"])
def front_page():
	resturants = Resturant.query.all()
	of_the_day = {
		"resturant": {},
		"reviews": {}
	}
	rest_array = []
	rev_array = []
	for r in resturants:
		rest_array.append(r.json_view())
		if r.of_the_day == True:
			of_the_day["resturant"] = r.json_view()
			r_query = Review.query.filter_by(ReviewTarget=r).all()
			for q in r_query:
				rev_array.append(q.json_view())
			of_the_day["reviews"] = rev_array
	return jsonify(resturants=rest_array, of_the_day=of_the_day)


@app.route('/full_view/<name>', methods=["GET", "POST"])
def full_view(name):
	name.replace("%20", " ")
	resturant = Resturant.query.filter_by(name=name).first();
	reviews = Review.query.filter_by(ReviewTarget=resturant).all()
	review_array = []

	for r in reviews:
		review_array.append(r.json_view())

	return jsonify(resturant= resturant.json_view(), reviews= review_array)


#developer use routes

@app.route('/add', methods=['GET', 'POST'])
def add_resturants():
	form = ResturantForm()
	if form.validate_on_submit():
		name = form.name.data
		price = form.price.data
		quality = form.quality.data
		type = form.type.data
		community_score = form.community_score.data
		expert_score = form.expert_score.data
		description = form.description.data
		rest = Resturant(name=name,price=price,quality=quality, type=type, community_score=community_score, description=description, expert_score=expert_score)
		db.session.add(rest)
		db.session.commit()

	return render_template('add.html', title="secret", form=form)

@app.route('/review', methods=['GET', 'POST'])
@login_required
def post_review():
	user = g.user
	name = user.name
	review = request.form['review_box']
	resturant = request.form['review_target']
	rev = Review(author=name, resturant=resturant, text=review)
	db.session.add(rev)
	db.session.commit()
	print request.form['destination']
	return redirect("#" + request.form['destination'])

@app.route('/clear')
def clear():
	logout_user()
	users = User.query.all()
	resturants = Resturant.query.all()
	reviews = Review.query.all()

	for u in users:
		db.session.delete(u)

	for rs in resturants:
		db.session.delete(rs)

	for rv in reviews:
		db.session.delete(rv)

	db.session.commit()
	return "All Clear"

@app.route('/otd', methods=['GET','POST'])
def of_the_day():
	form = OTDForm()
	resturant_otd = Resturant.query.filter_by(of_the_day=True).first()
	if request.method == 'POST':
		if resturant_otd:
			resturant_otd.of_the_day = False
			db.session.add(resturant_otd)
		new_otd = Resturant.query.get(form.of_the_day.data)
		new_otd.of_the_day = True
		db.session.add(new_otd)
		db.session.commit()

	return render_template('of_the_day.html', form=form)

@app.route('/count')
def user_num():
	return jsonify(usr=User.query.count(), resturants=Resturant.query.count(), reviews=Review.query.count())

@app.route('/reviews')
def reviews():
	reviews = Review.query.all();
	reviewList = []
	for r in reviews:
		reviewList.append(r.json_view())
	return jsonify(reviews=reviewList)

