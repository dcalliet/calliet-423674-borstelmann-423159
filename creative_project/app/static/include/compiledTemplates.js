this["TEMPLATES"] = this["TEMPLATES"] || {};

this["TEMPLATES"]["app/templates/header.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  return "	<button type=\"button\" class=\"btn btn-danger logout\">\n	<p>Logout</p>\n";
  },"3":function(depth0,helpers,partials,data) {
  return "		<button type=\"button\" class=\"btn btn-sm btn-warning login\">\n		Login\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<nav class=\"navbar navbar-inverse navbar-static-top\" role=\"navigation\">\n	<a href=\"#\">\n		<div class=\"container-fluid col-md-1\">	\n			<img src=\"/static/img/grub.png\">\n		</div>\n		<div class=\"container col-md-2\">\n			<p class=\"header\">grubconnoisseur.com</p>\n		</div>\n	</a>\n	<div class=\"container col-md-8\">\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.login : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(3, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		</button>\n	</div>\n</nav>";
},"useData":true});



this["TEMPLATES"]["app/templates/index.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  return "selected='selected'";
  },"3":function(depth0,helpers,partials,data) {
  return "checked=\"checked\"";
  },"5":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "		<div class=\"index-resturant-item\">\n			<a href=\"#profile/"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\" style=\"color:red\">"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</a><br>\n		</div>\n";
},"7":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "		<div class=\"index-reviews-item\">\n			<p>"
    + escapeExpression(((helper = (helper = helpers.author || (depth0 != null ? depth0.author : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"author","hash":{},"data":data}) : helper)))
    + " says:</p>\n			<p>"
    + escapeExpression(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"text","hash":{},"data":data}) : helper)))
    + "</p>\n		</div>\n";
},"9":function(depth0,helpers,partials,data) {
  return "		<a>No Reviews Posted</a>\n";
  },"11":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "	<form id=\"review\" name=\"review\" action=\"review\" method=\"POST\">\n		<textarea name=\"review_box\" rows=\"5\" cols=\"50\" placeholder=\"write a review\"></textarea>\n		<input name=\"review_target\" type=\"hidden\" value="
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.of_the_day : depth0)) != null ? stack1.resturant : stack1)) != null ? stack1.id : stack1), depth0))
    + ">\n		<input name=\"destination\" type=\"hidden\" value=\"index\">\n		<button class=\"btn btn-sm btn-block\" style=\"width:90%\" type=\"submit\">Submit</button>\n	</form>\n";
},"13":function(depth0,helpers,partials,data) {
  return "	<h3> Please Login to leave a comment </h3>\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<div class=\"panel-left\">\n\n	<select id=\"index_page_sort_select\">\n		<option value=\"name\" ";
  stack1 = ((helpers.equal || (depth0 && depth0.equal) || helperMissing).call(depth0, (depth0 != null ? depth0.sort_by : depth0), "name", {"name":"equal","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += ">Name</option>\n		<option value=\"price\" ";
  stack1 = ((helpers.equal || (depth0 && depth0.equal) || helperMissing).call(depth0, (depth0 != null ? depth0.sort_by : depth0), "price", {"name":"equal","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += ">Price</option>\n		<option value=\"quality\" ";
  stack1 = ((helpers.equal || (depth0 && depth0.equal) || helperMissing).call(depth0, (depth0 != null ? depth0.sort_by : depth0), "quality", {"name":"equal","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += ">Quality</option>\n		<option value=\"com_score\" ";
  stack1 = ((helpers.equal || (depth0 && depth0.equal) || helperMissing).call(depth0, (depth0 != null ? depth0.sort_by : depth0), "com_score", {"name":"equal","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += ">Community Score</option>\n		<option value=\"expert_score\" ";
  stack1 = ((helpers.equal || (depth0 && depth0.equal) || helperMissing).call(depth0, (depth0 != null ? depth0.sort_by : depth0), "expert_score", {"name":"equal","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += ">Expert Score</option>\n	</select>\n\n	<input type=\"checkbox\" ";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.decreasing : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " class=\"orientation\"> Decreasing</input><br>\n\n	<div class=\"index-resturant-container\">\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.resturants : depth0), {"name":"each","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "	</div>\n\n</div>\n\n<div class=\"panel-right\">\n	<h5>Resturant of the day: </h5>\n	<h5 class=\"resturant_title\">"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.of_the_day : depth0)) != null ? stack1.resturant : stack1)) != null ? stack1.name : stack1), depth0))
    + "</h5>\n	<p>                    \""
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.of_the_day : depth0)) != null ? stack1.resturant : stack1)) != null ? stack1.description : stack1), depth0))
    + "\"</p>\n	<br>\n	<h5>Reviews</h5>\n	<div class=\"index-reviews-container\">\n\n";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.of_the_day : depth0)) != null ? stack1.reviews : stack1), {"name":"each","hash":{},"fn":this.program(7, data),"inverse":this.program(9, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "	</div>\n\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.logged_in : depth0), {"name":"if","hash":{},"fn":this.program(11, data),"inverse":this.program(13, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\n</div>\n\n";
},"useData":true});



this["TEMPLATES"]["app/templates/profile.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "		<div class=\"profile-reviews-item\">\n			<p>"
    + escapeExpression(((helper = (helper = helpers.author || (depth0 != null ? depth0.author : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"author","hash":{},"data":data}) : helper)))
    + " says:</p>\n			<p>"
    + escapeExpression(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"text","hash":{},"data":data}) : helper)))
    + "</p>\n		</div>\n";
},"3":function(depth0,helpers,partials,data) {
  return "		<a>No Reviews Posted</a>\n";
  },"5":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda;
  return "	"
    + escapeExpression(((helper = (helper = helpers.debug || (depth0 != null ? depth0.debug : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"debug","hash":{},"data":data}) : helper)))
    + "\n		<form id=\"review\" name=\"review\" action=\"review\" method=\"POST\">\n		<textarea name=\"review_box\" rows=\"5\" cols=\"50\" style=\"width: 75%\" placeholder=\"write a review\"></textarea>\n		<input name=\"review_target\" type=\"hidden\" value="
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.resturant : depth0)) != null ? stack1.id : stack1), depth0))
    + ">\n		<input name=\"destination\" type=\"hidden\" value=\"profile/"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.resturant : depth0)) != null ? stack1.name : stack1), depth0))
    + "\">\n		<button class=\"btn btn-sm btn-block\" style=\"width:75%\" type=\"submit\">Submit</button>\n		</form>\n";
},"7":function(depth0,helpers,partials,data) {
  return "		<h3> Please Login to leave a comment </h3>\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, buffer = "<div class=\"profile-container\">\n	<div class=\"profile-title-div\">\n		<img src=\"/static/img/grub.png\">\n		<h1 class=\"resturant_title\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.resturant : depth0)) != null ? stack1.name : stack1), depth0))
    + "</h1>\n	</div>\n	<div class=\"profile-stats-div\">\n		<h5> Price: "
    + escapeExpression(((helpers.price || (depth0 && depth0.price) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? depth0.resturant : depth0)) != null ? stack1.price : stack1), {"name":"price","hash":{},"data":data})))
    + " </h5>\n		<h5> Type: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.resturant : depth0)) != null ? stack1.type : stack1), depth0))
    + " </h5>\n		<h5> Quality: </h5><div>"
    + escapeExpression(((helpers.rating || (depth0 && depth0.rating) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? depth0.resturant : depth0)) != null ? stack1.quality : stack1), {"name":"rating","hash":{},"data":data})))
    + "</div>\n		<h5> Customer Rating: </h5><div>"
    + escapeExpression(((helpers.rating || (depth0 && depth0.rating) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? depth0.resturant : depth0)) != null ? stack1.community_score : stack1), {"name":"rating","hash":{},"data":data})))
    + "</div>\n		<h5> Expert Rating: </h5><div>"
    + escapeExpression(((helpers.rating || (depth0 && depth0.rating) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? depth0.resturant : depth0)) != null ? stack1.expert_score : stack1), {"name":"rating","hash":{},"data":data})))
    + "</div>\n	</div>\n	<div class=\"profile-description-div\">\n		<p>\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.resturant : depth0)) != null ? stack1.description : stack1), depth0))
    + "\"</p>\n	</div>\n\n	<div class=\"profile-reviews-div\">\n\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.reviews : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.program(3, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "	</div>\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.logged_in : depth0), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.program(7, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>\n";
},"useData":true});