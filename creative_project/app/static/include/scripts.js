Handlebars.registerHelper("debug", function(optionalValue) {
  console.log("Current Context");
  console.log("====================");
  console.log(this);
  
  if (optionalValue) {
    console.log("Value");
    console.log("====================");
    console.log(optionalValue);
  }
});

Handlebars.registerHelper("price", function(price){
	var string = "";
	for (var x = 0; x < price; x++) {
		string = string + "$";
	}
	return string;
});

Handlebars.registerHelper("rating", function(rating){
	var string = "";
	for (var x = 0; x < rating; x++) {
		string = string + "<span class='glyphicon glyphicon-star'></span>";
	}

	for (var x = rating; x < 10; x++) {
		string = string + "<span class='glyphicon glyphicon-star-empty'></span>";
	}
	return  new Handlebars.SafeString(string);
});

Handlebars.registerHelper("equal", function(one, two, options){
	if (one == two){
		return options.fn(this);
	}else{
		return options.inverse(this);
	}
})
var ResturantModel = Backbone.Model.extend({
			url: '',
			initialize: function(){

			}
		});

var ResturantCollection = Backbone.Collection.extend({
			model: ResturantModel,
			url: '/resturants',
			parse: function(data){
				return data.resturants;
			}
		});

var FullViewData = Backbone.Collection.extend({
			model: ResturantModel,
			url: '/full_view',
		});

var Review = Backbone.Model.extend({
			url: '',
			initialize: function(){
				
			}
		});
var GrubRouter = Backbone.Router.extend({
			routes: {
				"about": "showAbout",
				"staff": "showStaff",
				"profile/:name": "showResturant",
				"": "showHome",
				"*other": "notFound"
			},
			showAbout: function(){

			},
			showStaff: function(){

			},
			showResturant: function(name){
				var profile = new ProfileView({'name':name});

			},
			showHome: function(){
				var index = new IndexView();

			},
			notFound: function(){
			}
		});
var HeaderView = Backbone.View.extend({
	el: "#header",
	initialize: function(){

		var xhr = new XMLHttpRequest();
		xhr.open("POST", "/check_login", false);
		xhr.send(null);
		index_data.logged_in = JSON.parse(xhr.response).logged_in;
		this.render();

	},
	events: {
		"click .login": "login",
		"click .logout": "logout"
	}, 
	render: function(){
		var content = TEMPLATES['app/templates/header.hbs'];
		var html = content({login: index_data.logged_in});
		this.$el.html(html);
		return this
	}, 
	login: function(e){
		var button = $(e.target).closest("button");
		window.location.href = "/login";
	}, 
	logout: function(e){
		var button = $(e.target).closest("button");
		button.removeClass("logout");
		button.addClass("login");
		var xhr = new XMLHttpRequest();
		xhr.open("POST", "/logout", false);
		xhr.send(null);
	}
})
var IndexView = Backbone.View.extend({
			el: "#content",
			initialize: function(){
				var _this = this;
				var xhr = new XMLHttpRequest();
				xhr.open("GET", "/front_page", false);
				xhr.addEventListener("load", function(event){
					index_data.of_the_day = JSON.parse(event.target.responseText).of_the_day;
					index_data.resturants = resturants_coll.toJSON();
					_this.render();
				}, false);
				xhr.send(null);
			},
			events: {
				"click .orientation": "orient",
				"change #index_page_sort_select": "select"
			},
			render: function(){
				var data = {
					resturants: {}
				}
				var resturants = _.sortBy(index_data.resturants, function(rest){
						if(index_data.sort_by == "name"){
							return rest.name;
						}else if(index_data.sort_by == "price"){
							return rest.price;
						}else if(index_data.sort_by == "quality"){
							return rest.quality;
						}else if(index_data.sort_by == "community_score"){
							return rest.community_score;
						}else if(index_data.sort_by == "expert_score"){
							return rest.expert_score;
						}else {
							return rest.name;
						}
				});

				if(index_data.decreasing){
					resturants.reverse();
				}
				index_data.resturants = resturants;

				var content = TEMPLATES['app/templates/index.hbs'];
				var html = content(index_data);
				this.$el.html(html);
				return this;
			},
			select: function(e){
				var target = $(e.target);
				index_data.sort_by = target[0].value;
				this.render();
			},
			orient: function(e){
				var target = $(e.target);
				if(target[0].checked == true){
					console.log("on");
					index_data.decreasing = true;
				}else{
					console.log("off")
					index_data.decreasing = false;
				}
				this.render();
			}

		});

var ProfileView = Backbone.View.extend({
			el: "#content",
			initialize: function(options){
				var param = options || {},
				_this = this;				
				this.model = {
					"resturant": {},
					"review": []
				}
				var xhr = new XMLHttpRequest();
				xhr.open("GET", "/full_view/" + param.name, false);
				xhr.addEventListener("load", function(event){
					_this.render(JSON.parse(event.target.responseText));
				}, false);
				xhr.send(null);
			},
			render: function(e){
				e.logged_in = index_data.logged_in;
				var content = TEMPLATES['app/templates/profile.hbs'];
				var html = content(e);
				this.$el.html(html);
				return this;
			}
		});