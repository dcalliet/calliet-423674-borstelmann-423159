Handlebars.registerHelper("debug", function(optionalValue) {
  console.log("Current Context");
  console.log("====================");
  console.log(this);
  
  if (optionalValue) {
    console.log("Value");
    console.log("====================");
    console.log(optionalValue);
  }
});

Handlebars.registerHelper("price", function(price){
	var string = "";
	for (var x = 0; x < price; x++) {
		string = string + "$";
	}
	return string;
});

Handlebars.registerHelper("rating", function(rating){
	var string = "";
	for (var x = 0; x < rating; x++) {
		string = string + "<span class='glyphicon glyphicon-star'></span>";
	}

	for (var x = rating; x < 10; x++) {
		string = string + "<span class='glyphicon glyphicon-star-empty'></span>";
	}
	return  new Handlebars.SafeString(string);
});

Handlebars.registerHelper("equal", function(one, two, options){
	if (one == two){
		return options.fn(this);
	}else{
		return options.inverse(this);
	}
})