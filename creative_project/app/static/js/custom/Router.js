var GrubRouter = Backbone.Router.extend({
			routes: {
				"about": "showAbout",
				"staff": "showStaff",
				"profile/:name": "showResturant",
				"": "showHome",
				"*other": "notFound"
			},
			showAbout: function(){

			},
			showStaff: function(){

			},
			showResturant: function(name){
				var profile = new ProfileView({'name':name});

			},
			showHome: function(){
				var index = new IndexView();

			},
			notFound: function(){
			}
		});