from flask.ext.wtf import Form
from wtforms import StringField, TextAreaField, BooleanField, IntegerField, DecimalField, DateField, SelectField
from wtforms.validators import DataRequired, Length
from models import Resturant
class LoginForm(Form):
	openid = StringField('openid', validators=[DataRequired()])
	remember_me = BooleanField('remember_me', default=False)

class ResturantForm(Form):
	name = StringField('name')
	price = IntegerField('price')
	quality = IntegerField('quality')
	type = StringField('type')
	description = TextAreaField('description')
	community_score = IntegerField('community_score')
	expert_score = IntegerField('expert_score')

class OTDForm(Form):
	resturants = Resturant.query.all()
	names = []
	for rs in resturants:
		names.append((rs.id, rs.name))

	of_the_day = SelectField('Resturant of the Day', choices=names)

class ReviewForm(Form):
	text = StringField('text')
	author = StringField('author')
	resturant = StringField('resturant')