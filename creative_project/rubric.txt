Grading Rubric

grubconnoisseur.com

5 points grading rubric submitted on time
10 points Usability
50 points Flask and Backbone web app
20 points creative
15 intuitive website that has nice CSS

Website for reviewing local restaurants in your area, starting in STL for this project.  There will be formal reviews and overviews about each establishment on the webpage but then users can submit feedback and comments and score the restaurants themselves.

TA: Adam Frank
